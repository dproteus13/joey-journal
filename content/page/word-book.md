---
title: Word Book
subtitle: Joey's Favorite Things!
comments: false
---

1. Mommy
1. Daddy
1. Artie
1. Gah & Gigah
1. Grumps & Nanny
1. Spyhop
1. Book
1. Elephant
1. Car
1. Swing
1. Outside
1. Cup
1. Sandbox / Sandtable / Sand
1. Shoes
1. Monkey Garage
1. Piano
1. Harmonica
1. Do-Do
1. Markers
1. Crayons
