---
title: Vocabulary
subtitle: Growing Every Week!
comments: false
---

1. up
1. down
1. yes
1. no
1. blue
1. purple (was urple, now purple)
1. mommy
1. daddy
1. mom
1. dad
1. in
1. outside (ousside)
1. ball
1. cup (up-a)
1. whale (when prompted by Artie)
1. RaRa (his name for Artie)
1. Gigah
1. one
1. two
1. three
1. five
1. six
1. eight
1. plate
1. bowl
1. cheese
1. nothing (just heard this one once)
1. monkey (referring to the monkey garage)
1. me
1. heart
1. pull
1. please (peas)
1. yay
1. cat
1. out
1. moon (on its way)
1. fly (like the bug)
1. hat
1. throw
1. poop
1. phone
1. kick
1. eat
1. shoes
1. Elmo
1. Mickey
1. stool
1. bubble
1. goofy
1. fish
1. open (bee-ahhh, now able to say "open")
1. Joey
1. tree
1. orange
1. saw
1. more (mee-ahhh)
1. apple
1. closed
1. Jackie
1. Sam
1. Marshall (as in the Pup)
1. zoo
1. I
1. away
1. baby
1. banana (nana)
1. turn
1. hotdog
1. home
1. eyes
1. ears
1. press (sounding much better now)
1. boooooo (like a ghost!)
1. sheep
1. tea
1. bee
1. beep
1. love (sounds more like "wuv")
1. you
1. song
1. play
1. top
1. this
1. door
1. chair
1. choo choo
1. Joe
1. chip
1. yuck
1. Nanny
1. Grumps
1. trees
1. hammer
1. butterfly
1. pouch
1. cream
1. thumb
1. hand
1. cheerio
1. Tommy
1. spin
1. wee wee (has had this for a while, means "spin and around")
1. house
1. sunny
1. applesauce
1. jump
1. see
1. Santa
1. sit
1. love
1. lid
1. here
1. want
1. duck
1. Minnie
1. Donald
1. Goofy
1. Pluto
1. Daisy
1. pepper
1. peas
1. zipper
1. push
1. okay
1. table
1. pick
1. doll
1. wand
1. alligator
1. iguana
