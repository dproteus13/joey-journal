---
title: Phrases
subtitle: 3+ word phrases
comments: false
---

1. Daddy, more please
1. Daddy, help please
1. I love you (mumble-ish, but we heard it!)
1. I see Daddy!
1. I see a tree outside!
1. Daddy, come please.
1. I want ball
1. Daddy, throw here
1. I want green
1. Daddy, wipe please.
1. chip pick up
1. I want wand
1. I see whale
1. I see door
1. More please, Mommy!
1. More please, Daddy!
1. Farm book please, Mommy!
