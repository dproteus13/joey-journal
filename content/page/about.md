---
title: About me
subtitle: Tracking Joey's Progress
comments: false
---

This space is here to help track Joey's progress through DI and will hopefully
go away soon!

I'm also working on a script, on the side, that will pull data about Joey's
progress from the git history to help visualize his progression over time.

