---
title: Session 26
date: 2020-03-01
---

Joey was listening to Yellow Submarine when Ms. Chris pulled up, and the Ring
announcement interrupted the song.  He was upset until he realized it was Ms.
Chris, and then he excitedly yelled "Ms. Chris YAY!"

They played with the Marble Run and Joey had to say "I put marble in" before he
was allowed to put the marble in.  They worked on sentences as Joey wanted more
marbles, so he was saying "I want three marbles."


