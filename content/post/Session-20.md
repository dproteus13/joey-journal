---
title: Session 20
date: 2020-01-13
---

When Ms. Chris came in the door, Joey got very excited, yelled "table!" and
moved the table over to their normal spot.

They started playing with the "Let's Go Fishin'" game, and Joey was very excited
that he caught a Blue Fish, but we couldn't quite get him to say it.

Next they got out the "WH'Chipper Chart" - which Joey was very interested in how
the magnet worked.  They placed magnetic disks on different parts of a game
board, naming the items they were covering with the chips.

Ms. Chris noticed Joey's conversational use of "okay" and was impressed!

