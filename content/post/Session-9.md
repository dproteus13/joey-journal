---
title: Session 9
date: 2019-10-21
---

Joey was very excited to play with Ms. Chris and the Lucky Ducks game, but then
he quickly lost interest and wanted to play with the Ring Toss.

Joey was able to name the colors of the rings with some help, and then told us
when he wanted to put things "away."

Next Ms. Chris read Joey a book about a mommy dog looking for her baby.  He was
working on a few of the different animals that she found, but got a very good
"baby" by the end of the book.

Pop-tubes and flashing magic wands!

They moved on to flashcards (from the Headbanz game!), and then Joey was
interested in the rubber band holding the cards together.  Ms Chris showed him
how to make a musical instrument by putting the rubber band across 2 dowels and
strumming it.

After cleaning up, Joey got to play with Fidget Spinners on the table,
practicing "go," "slow," "fast," "spin," "stop," and "off."
