---
title: Session 19
date: 2020-01-11
---

Make up session today since Joey was sick last Monday.

Ms. Chris brought a bin of play food, and Joey was excited to name some of his
favorite foods, including "pepper" and "peas"!

* [ ] Buy a pepper to help Joey practice the repetitive motor movement

Next Joey practiced naming the foods as he put them back in the bin, "pepper in"
etc.

Joey helped himself to the bag of toys and then found a fisher price tablet,
then got out the paint can.  He was very good at naming the numbers, and
impressed Ms. Chris a lot!

