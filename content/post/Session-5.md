---
title: Session 5
date: 2019-09-23
---

Joey started off by showing off his fantastic pencil grip on the Water Wow
paintbrush!

Joey was happy and cooperative, but very quiet.  Mommy had warned that he had a
slight fever earlier in the day, and was not quite himself.

Went through several different toys / games to find something that would bring
Joey's words out.

As he was putting away Legos, Ms. Chris taught Joey how to say "throw" and throw
the legos into their bag.

Joey told us that he needed to "poop" very clearly, and Daddy took him to the
potty.  We tried sitting on the toilet, but Joey was very upset about trying
that so we got back down and he washed his hands.

Joey demonstrated a three-point pencil grip with crayons today!

Our word to practice this week is "throw"

