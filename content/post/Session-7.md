---
title: Session 7
date: 2019-10-07
---

Daddy showed up with only a few minutes left in the Session, many thanks to Gah
and Gigah for getting Joey from school and starting things off!

Joey had to be woken up from his nap in order to get to home for his session,
and was tired throughout.

Joey was playing with "Ned's Head" and the "Go Fish" game, but he did not like
feel of squishy dinosaurs.

Joey was much more vocal while everyone else was talking amongst themselves, and
Joey was not the center of attention.  He was also much more vocal while he was
distracted, rather than when we were trying to extract words from him.

Ms. Chris's 0800 Monday child is going to be aging out of the program soon, so we might
be able to adjust Joey to take that time slot.

