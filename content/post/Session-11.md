---
title: Session 11
date: 2019-11-04
---

Joey was very upset when Artie left this morning, but as soon as Daddy mentioned
that Ms. Chris was coming he got very excited!

When Ms. Chris got here, she and Joey started painting, and Joey quickly got
interested in the water cup's "top," which Ms. Chris was very excited to hear.

Ms. Chris wants to look at working on Joey's pencil grip, she suggested talking
to Ms. Taryn and asking at what point we should worry about his 3-point pencil
grip, etc.

After they painted a bit, Joey got to pull toys out of the Mystery Box!  He was
very excited to pull things "out!"  He got several new words in while playing
with the items from the Mystery Box.

Next Joey and Ms. Chris played with Spot It cards, naming the items on the
cards.

* [ ] Try to stress open-ended questions (ie. "Do you want this or this?"
  instead of yes or no questions)
* [ ] Try to use car time to point out things that we see outside, and asking
  Joey what he sees outside: "Joey, do you see that girl in the pink dress?"
* [ ] Pick 5 words that Joey already uses, and try to put them into multi-word
  phrases throughout the week: "Slide down!" etc.
