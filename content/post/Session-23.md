---
title: Session 23
date: 2020-02-10
---

Before today's session, Daddy got adventurous and tried to let Joey paint with
the Color Wonder set, but when Daddy turned his back Joey took the paintbrush
apart and dumped water all over everything!

When Ms. Chris got here, Joey started raiding her bag and found the Connect Four
game.  They practiced putting the chips in while saying "I put red chip in," "my
turn," and "I want black chip please."

* [ ] Start trying to stress the inclusion of verbs into Joey's sentences.

Joey started to get frustrated asking, and started trying to just walk around
and steal the chips!

Next they started to play with Ms. Chris's new Water Wonder set that she just
got from Amazon :).

* [ ] Keep a close eye on Joey's "k" sounds ("cat," "cup," "car," "card," etc.),
  it almost sounds like they are starting to soften.

Next they played with some Monkeys in a Barrel, practicing saying "hook" as they
attached new monkeys to the chain.  Joey started to get very interested in the
monkey relief on the top of the barrel, and tried to figure out how to attach a
new monkey to it.  Then he opened the barrel and started putting the monkeys
away.

Daddy asked about Joey's new habit of spitting out his food, Ms. Chris suggested
we look for possible new teeth or any reason it might be painful for him to be
biting more.
