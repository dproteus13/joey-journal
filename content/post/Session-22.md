---
title: Session 22
date: 2020-02-03
---

Today Joey and Ms. Chris played with a Magic Track car, and Joey practiced
saying "Car on track" while keeping his tongue back inside his mouth.

Next they played with the Water Wow cards some more, and Joey was very excited
to tell me about the pictures he found, and the numbers!

Then they played with some cards that had tactile swatches with animals (bumpy
frog, soft duck, etc.) and another set of cards that had pictures that pulled
out (keys, plane, etc.).

Next they played with the matching cards, at first Joey was good about getting
a match, giving them to Ms. Chris, and saying what he was giving her.  He seemed
to get tired though, and stopped talking at all but was instead just giving her
the matches.
