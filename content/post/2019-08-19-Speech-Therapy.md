---
title: First post!
date: 2019-08-19
---

Note about notes from Ms. Chris:

1. /errrr/ -> Sounds that we should demonstrate for Joey
1. "two"   -> Words / sounds that Joey has demonstrated during the session

Homework for Mom and Dad:

1. List out all of the words that Joey knows how to say, keep track of things he
says regularly (as part of his vocabulary) vice words
1. Mom: /M/ /mmmmm/
1. Dad: /R/ /errrr/
1. At bed time, try to get him to say the name of the stuffed animal that he wants
to snuggle with.  Ask him to get the animal he wants and then name it.

