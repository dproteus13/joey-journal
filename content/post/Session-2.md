---
title: Session 2
date: 2019-08-26
---

Gigah had to sub in for Daddy, thankfully Joey had Great Behavior on his report!

Joey played with water paint.  Gigah reported that he had clammed-up, somewhat,
unfortunately :speak_no_evil:.

Homework for Mom and Dad:

1. Create a Photobook of Joey's favorite things, show it to him throughout the
   day, and at bedtime.
1. Pick 1 or 2 songs and bombard Joey with them.  Play / sing songs with dinner and
   lunch, during his bath, and before bedtime.

Next session will be September 9th, but Chris is available on September 7th for
a makeup session from Labor Day, if that works for us.

