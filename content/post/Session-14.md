---
title: Session 14
date: 2019-11-25
---

Joey was so excited that he was climbing the couch to watch for Ms. Chris until
she arrived!  Artie was also with us this morning.

The boys practiced taking turns and "throw"ing darts at the dart board.

Next Joey and Ms. Chris played with Fidget Spinners and practiced saying "I
spin", "Daddy spin", etc.

Next they played with Zingo!

Then the cars came out, and we realized that Joey hasn't really said the word
*car* yet.  Ms. Chris gave Artie homework to help teach Joey the word *car*.
