---
title: Session 4
date: 2019-09-16
---

Daddy was distracted for the first half of our session with a contractor, but
Ms. Chris was playing play-doh and Zingo! with Joey!

New words this session:

* cat
* out

Try to pick a word from his existing vocabulary, and try to use that word a lot
so that we can embed the word in his vocabulary.

Played flashcards and learned a few new words!

* moon
* fly
* hat

