---
title: Session 13
date: 2019-11-16
---

Joey was so excited to read his "Joey Picture Book" to Ms. Chris!

Next we all played Connect Four announcing "My Turn" or "Daddy's Turn", as well
as "Chip In!" as we inserted the chips.

* [ ] Mommy and Daddy pick 3 phrases each to try to bombard Joey with throughout
  the week.  Try to focus on p's, b's, w's, and d's.

Next Joey found some bubbles in the side of Ms. Chris's bag, so we talked about
"bubbles" for a little while.  Joey got a good "bubble" after a little
prompting.

Next we got out the big bag of dinosaurs, but we need to work on getting Joey to
be able to make the beginning part of the word: /dine/.
