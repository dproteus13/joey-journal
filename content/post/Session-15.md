---
title: Session 15
date: 2019-12-02
---

Joey heard Ms. Chris's car door, and started excitedly cleaning up the couch
when Daddy confirmed that Ms. Chris was here!

Ms. Chris brought a ball game pushing the balls into holes at the top, and they
come out at the bottom.  He practiced saying "Ball In" and "Ball Out" to
describe what was happening.

* [ ] Try to focus on getting Joey to say 2 words together every time he says
  anything.
* [ ] Also, select 3 ornaments from the tree that are within Joey's reach, and
  describe them to him, and work on getting him to name them.

Then they played with play-doh, "press"ing "cow" "giraffe" and "heart" molds
into the play-doh.  Joey got a very good "pr" in his "press" which was exciting!

With the play-doh press / fun factory, Joey got to practice cutting, and was
very happy to narrate "I cut" as he did so!  He also narrated "I press" as he
was pressing down on the fun factory.

Joey was very interested in the Heart, and wanted to "press" and "pull" that
mold over and over.  He also got excited to "press hard" a Christmas-tree mold.

* Lourdes Early Intervention Holiday Party this Saturday 12/7, let Ms. Chris
  know by Wednesday if we're going to be able to make it.

After cleaning up the Play-Doh, Joey got to play with the MagnaDoodle.

* [ ] Work on getting Joey to bring his mouth forward, like for the word "want,"
  could also be "whooo" like an owl, but not "hoot" like an owl.
