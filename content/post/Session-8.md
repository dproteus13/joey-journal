---
title: Session 8
date: 2019-10-14
---

Ms. Chris came earlier in the day, so that she could see Joey when he hadn't
just woken up from a nap.  He was very excited to see her, and eagerly cleaned
off his table to play with her.

Joey got to play with Dot-Dot markers!

Ms. Chris also had a toolkit, and Joey was very excited to be putting the tools
together, and figuring out how they work.

* [ ] Mommy and Daddy should try to highlight when they "Stop" and "Go"
  (especially in the car)!

