---
title: Session 10
date: 2019-10-28
---

Joey was super excited to show Ms. Chris his Busy Farm book this morning, and
wouldn't put it away before she came.

After he showed Ms. Chris the book, they started playing with stamps, naming all
kinds of different vehicles.  They also had to "Tap" the stamps against the ink
pads to make them work!

When he was finished, Joey started to put the stamp he was holding away, and
said *way* for "away".

Then they started playing with Mr. Potato-Head, so Joey could practice the names
of body parts, and work on his "p."  He got a few new 2 word phrases this way!

Then Joey got to play with Ms. Chris's cars, but first he had to work on saying
"open bag" but he didn't quite get there (he got too frustrated and excited
about the cars).

Ms. Chris suggested that Joey's language skills would flourish during a vacation
in a Camper!

Next Ms. Chris did some flashcards as well, and Joey got several new words!
