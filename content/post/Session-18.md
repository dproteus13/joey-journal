---
title: Session 18
date: 2019-12-30
---

Daddy missed most of last session, Mommy took notes on a dead tree!

Ms. Chris was greeted like a celebrity by both Joey and Artie today!  She set up
Lucky Ducks and Joey, Mommy, Artie, and Ms. Chris played for a few minutes,
taking turns and practicing stringing multiple words together.

Goal for January is going to be to have Joey regularly stringing 3 words
together.

* Ms Chris also said we should try to gently correct Joey's protruding tongue,
  before that develops into a strong habit.

Joey suddenly decided he was done with Lucky Ducks, and started cleaning it all
up despite Ms. Chris wanting to play a little bit longer.

Next they played with the I SPY Preschool Game, and Joey was able to point to
the items and say "ball here!"

Again, Joey got suddenly finished with the game, and started putting it away.
Then he started routing through Ms. Chris's bag of tricks.
