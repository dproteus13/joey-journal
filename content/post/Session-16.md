---
title: Session 16
date: 2019-12-09
---

Ms. Chris brought her ring-toss game, and Joey was very excited to throw the
rings on the post.  He took turns asking "Daddy, throw here" and "Joey throw."

After the ring toss, Joey got to paint with water and practiced saying "dip,
dip, dip!"

* [ ] Work on getting Joey to make an "rrrrr" sound.  Maybe with the sound of a
  tiger "roooaaar".  Also with "car" and "red."

Then Joey was playing with the spinners and tops, we need to work on saying
"Around."  Then Joey was playing with a squishy house and we realized we need to
work on his 'h' sound too, so we need to work on "Ho! Ho! Ho!" and "home".
