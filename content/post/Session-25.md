---
title: Session 25
date: 2020-02-24
---

Joey was very excited to see Ms. Chris when she arrived, and immediately asked
to play with chips (Connect Four).  He got tired of it fairly quickly, though,
because we were trying to make him talk and he didn't want to say "Thank You"

Next they played with a set of velcro mitts and a ball (can't remember the name
of the set).  Joey had a strong arm, but started wanting to through the ball all
around the house so we put those away and got out the Water Wonder set.

When he was done with painting, Joey said "I want cars, Chris, please"
