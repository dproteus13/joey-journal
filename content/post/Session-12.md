---
title: Session 12
date: 2019-11-11
---

Joey was not feeling great today, he had a bit of a rough night last night.
When Ms. Chris showed up, though, he was very excited to see her!

Joey and Ms. Chris played with PlayDoh, practicing "cut", "press", and "open" as
well as shapes like "heart" and "tree".  He also made a "whale" and an
"elephant"!  Then they made a "pig" to show GiGah (Daddy has a picture)!

When they were done playing with PlayDoh, Joey went fishing!  After he got all
of the fish out with the magnetic rod, he was able to put away 7 of the 10 fish
in the puzzle without any help at all!

Next up was the I Spy books for kids, and Joey learned how to say "hammer"!  He
was also very excited when Ms. Chris asked him to help find the red firetruck,
though he quickly lost interest again.
